//
// Created by sg on 15.09.2019.
//

#include "../MapReduce/MapReduceSpecification.hpp"
#include "../MapReduce/WorkerNode.hpp"

#include <gtest/gtest.h>

using InputType = std::tuple<size_t, size_t, std::vector<std::string>,
                             std::vector<std::string>, std::string>;

class WordCounterTest : public ::testing::TestWithParam<InputType> {};

TEST_P(WordCounterTest, WordCounterTest_W_Test) {
  auto [process_num, reduce_num, input_files, output_files, answer_file] =
      GetParam();

  MapReduceSpecification spec(Mapper::MapperType::WordCount,
                              Reducer::ReducerType::WordCount, process_num,
                              reduce_num);
  for (auto &output : output_files)
    spec.AddOutput(output);

  for (auto &input : input_files)
    spec.AddInput(input);

  spec.SetCombinerType(Reducer::ReducerType::WordCount);

  spec.MapReduce();

  std::map<std::string, std::string> answer;

  std::ifstream ans_ifs(answer_file);

  while (!ans_ifs.eof()) {
    std::string key, value;
    ans_ifs >> key >> value;
    if (key.empty() || value.empty())
      continue;
    answer.emplace(std::move(key), std::move(value));
  }
  ans_ifs.close();
  size_t key_size = answer.size();

  for (auto &output : output_files) {
    std::ifstream ifs(output);
    while (!ifs.eof()) {
      std::string key, value;
      ifs >> key >> value;
      if (key.empty() || value.empty())
        continue;
      ASSERT_TRUE(answer[key] == value);
      --key_size;
    }
    ifs.close();
  }
  ASSERT_TRUE(key_size == 0) << "But returned " << key_size;
}

using Vector = std::vector<std::string>;

INSTANTIATE_TEST_CASE_P(INST_WORD_COUNT, WordCounterTest,
                        ::testing::Values(InputType(
                            10, 3, Vector{"../test/ABbc.txt"},
                            Vector{"./out1.txt", "./out2.txt", "./out3.txt"},
                            std::string("../test/answer.txt"))));
