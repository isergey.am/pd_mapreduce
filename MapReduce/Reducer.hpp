//
// Created by sg on 13.09.2019.
//

#pragma once

#include "ReduceInputIterator.hpp"
#include <functional>
#include <string>

class Reducer {
public:
  virtual void Reduce(ReduceInputIterator &input) = 0;

  void Emit(const std::string &value);

  enum class ReducerType { WordCount };

private:
  using callback_t = std::function<void(const std::string &)>;

  void SetEmitCallback(callback_t callback);

private:
  callback_t emit_callback;

  friend class WorkerNode;
};
