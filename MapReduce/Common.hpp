//
// Created by sg on 14.09.2019.
//

#pragma once

#include <string>
#include <vector>

class Common {
public:
  static std::vector<std::string> SplitString(const std::string &str);
};
