//
// Created by sg on 14.09.2019.
//

#include "Common.hpp"
#include <iterator>
#include <sstream>

std::vector<std::string> Common::SplitString(const std::string &str) {
  std::istringstream iss(str);
  std::vector<std::string> split_arr{std::istream_iterator<std::string>(iss),
                                     {}};
  return split_arr;
}
