//
// Created by sg on 14.09.2019.
//

#include "MapReduceSpecification.hpp"
#include <cassert>
#include <filesystem>
#include <iostream>
#include <iterator>

namespace fs = std::filesystem;

void MapReduceSpecification::SetMapperType(Mapper::MapperType mapperType) {
  mapper_type_ = mapperType;
}

void MapReduceSpecification::SetReducerType(Reducer::ReducerType reducerType) {
  reducer_type_ = reducerType;
}

void MapReduceSpecification::SetCombinerType(
    Reducer::ReducerType combinerType) {
  combiner_type_ = std::make_unique<Reducer::ReducerType>(combinerType);
}

void MapReduceSpecification::AddInput(std::string input_file) {
  input_files_.push_back(std::move(input_file));
}

void MapReduceSpecification::AddOutput(std::string output_file) {
  output_files_.push_back(std::move(output_file));
}

void MapReduceSpecification::MapReduce() {
  assert(output_files_.size() == reducer_num_);
  std::vector<std::string> split_files;

  for (auto &input_file : input_files_) {
    SplitFile(input_file, split_files);
  }

  RunWithMpi(split_files);

  for (auto &file : split_files)
    fs::remove(file);
}

const size_t MapReduceSpecification::ChunkSize = 10000;

void MapReduceSpecification::SplitFile(const std::string &file,
                                       std::vector<std::string> &split_files) {
  std::ifstream ifs(file);
  std::vector<char> buffer(ChunkSize + 1);

  while (!ifs.eof()) {
    ifs.read(buffer.data(), ChunkSize);
    std::string rest;
    std::getline(ifs, rest, ' ');
    char file_name[L_tmpnam];
    std::ofstream ofs(std::tmpnam(file_name));
    ofs.write(buffer.data(), ifs.gcount());
    ofs << rest;
    ofs.close();
    split_files.emplace_back(file_name);
  }
}

void MapReduceSpecification::RunWithMpi(
    const std::vector<std::string> &split_files) {
  std::string cmd =
      "mpirun -np " + std::to_string(process_num_) + " ./MapReduce " +
      std::to_string(reducer_num_) + " " +
      std::to_string(static_cast<int>(mapper_type_)) + " " +
      std::to_string(static_cast<int>(reducer_type_)) + " " +
      std::to_string(
          (combiner_type_ ? static_cast<int>((*combiner_type_)) : -1));

  std::stringstream str;
  std::copy(split_files.begin(), split_files.end(),
            std::ostream_iterator<std::string>(str, " "));
  std::copy(output_files_.begin(), output_files_.end(),
            std::ostream_iterator<std::string>(str, " "));

  cmd += " " + str.str();
  std::cout << cmd << std::endl;
  if (system(cmd.c_str()) != 0)
    exit(1);
}

MapReduceSpecification::MapReduceSpecification(Mapper::MapperType mapperType,
                                               Reducer::ReducerType reducerType,
                                               size_t processNum,
                                               size_t reducer_num)
    : mapper_type_(mapperType), reducer_type_(reducerType),
      process_num_(processNum), reducer_num_(reducer_num) {
  assert(process_num_ >= reducer_num_);
}
