//
// Created by sg on 14.09.2019.
//

#pragma once

#include "Reducer.hpp"

class WordCountReducer : public Reducer {
public:
  void Reduce(ReduceInputIterator &input) override;
};
