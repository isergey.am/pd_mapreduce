//
// Created by sg on 14.09.2019.
//

#pragma once

#include "Mapper.hpp"

class WordCountMapper : public Mapper {
public:
  void Map(const std::string &data) override;
};
