//
// Created by sg on 14.09.2019.
//

#include "MasterNode.hpp"
#include "WordCountMapper.hpp"
#include "WordCountReducer.hpp"
#include "WorkerNode.hpp"
#include <cassert>

int main(int argc, char **argv) {

  MPI_Init(&argc, &argv);

  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  if (world_rank == 0) {
    size_t reduce_num = strtoul(argv[1], nullptr, 10);
    std::array<int, 4> params;
    params[0] = strtol(argv[2], nullptr, 10);
    params[1] = strtol(argv[3], nullptr, 10);
    params[2] = strtol(argv[4], nullptr, 10);
    params[3] = reduce_num;
    MPI_Bcast(params.data(), 4, MPI_INT, 0, MPI_COMM_WORLD);
    std::vector<std::string> input_files;
    std::vector<std::string> output_files;
    for (size_t i = 5; i < argc - reduce_num; ++i)
      input_files.emplace_back(argv[i]);

    for (size_t i = argc - reduce_num; i < argc; ++i)
      output_files.emplace_back(argv[i]);

    MasterNode node(MPI_COMM_WORLD, reduce_num, std::move(input_files),
                    std::move(output_files));
    node.Start();
  } else {
    std::array<int, 4> params;

    MPI_Bcast(params.data(), 4, MPI_INT, 0, MPI_COMM_WORLD);

    auto [mapper_type, reducer_type, combiner_type, reduce_num] = params;

    Mapper *mapper{nullptr};
    Reducer *reducer{nullptr};
    Reducer *combiner{nullptr};

    switch (mapper_type) {
    case static_cast<int>(Mapper::MapperType::WordCount):
      mapper = new WordCountMapper();
      // TODO:: more mappers
    }

    switch (reducer_type) {
    case static_cast<int>(Reducer::ReducerType::WordCount):
      reducer = new WordCountReducer();
      break;
      // TODO:: more reducers
    }

    switch (combiner_type) {
    case static_cast<int>(Reducer::ReducerType::WordCount):
      combiner = new WordCountReducer();
      break;
      // TODO:: more combiners
    }

    assert(mapper && reducer);
    WorkerNode node(mapper, reducer, combiner, MPI_COMM_WORLD, reduce_num);
    node.Start();
  }
  MPI_Finalize();
  return 0;
}
