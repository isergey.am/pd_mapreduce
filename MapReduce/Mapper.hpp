//
// Created by sg on 13.09.2019.
//

#pragma once

#include <functional>
#include <string>

class Mapper {
public:
  virtual void Map(const std::string &data) = 0;

  void Emit(const std::string &key, const std::string &value);

  enum class MapperType { WordCount };

private:
  using callback_t =
      std::function<void(const std::string &, const std::string &)>;

  void SetEmitCallback(callback_t callback);

private:
  callback_t emit_callback;

  friend class WorkerNode;
};
