//
// Created by sg on 13.09.2019.
//

#include "BufferedLineReader.hpp"

BufferedLineReader::BufferedLineReader(std::string file_name)
    : file_name_(std::move(file_name)), ifs_(file_name_), buffer_(BufferSize) {}

std::string BufferedLineReader::ReadLine() {
  if (Done())
    return "";

  if (buffered_lines_.empty())
    FillBuffer();

  if (buffered_lines_.empty())
    return "";

  auto str = std::move(buffered_lines_.front());
  buffered_lines_.pop_front();
  return str;
}

void BufferedLineReader::FillBuffer() {
  ifs_.read(buffer_.data(), BufferSize);
  size_t end = ifs_.gcount();
  size_t cur = 0;
  while (cur < end) {
    size_t line_end = cur;
    while (line_end < end && buffer_[line_end] != '\n')
      ++line_end;
    if (line_end == end)
      --line_end;
    buffered_lines_.emplace_back(buffer_.data() + cur, line_end - cur + 1);
    cur = line_end + 1;
  }
  if (!buffered_lines_.empty() && buffered_lines_.back().back() != '\n' &&
      !ifs_.eof()) {
    std::string str;
    std::getline(ifs_, str, '\n');
    buffered_lines_.back() += str;
  }
}

bool BufferedLineReader::Done() {
  return buffered_lines_.empty() && (ifs_.eof() || ifs_.fail());
}

void BufferedLineReader::GiveBack(std::string line) {
  buffered_lines_.push_front(std::move(line));
}

void BufferedLineReader::Close() { ifs_.close(); }

const size_t BufferedLineReader::BufferSize = 1000;
