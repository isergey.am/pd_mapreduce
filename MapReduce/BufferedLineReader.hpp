//
// Created by sg on 13.09.2019.
//

#pragma once

#include <deque>
#include <fstream>
#include <string>
#include <vector>

class BufferedLineReader {
public:
  explicit BufferedLineReader(std::string file_name);

  std::string ReadLine();

  bool Done();

  void GiveBack(std::string line);

  void Close();

private:
  void FillBuffer();

private:
  static const size_t BufferSize;
  std::deque<std::string> buffered_lines_;
  std::string file_name_;
  std::ifstream ifs_;
  std::vector<char> buffer_;
};
