//
// Created by sg on 12.09.2019.
//

#include "MasterNode.hpp"
#include "Common.hpp"
#include <cassert>
#include <filesystem>
#include <future>
#include <iterator>
#include <sstream>

namespace fs = std::filesystem;

MasterNode::MasterNode(MPI_Comm communicator, size_t reduced_num,
                       std::vector<std::string> input_files,
                       std::vector<std::string> output_files)
    : communicator_(communicator), reducer_num_(reduced_num),
      input_files_(std::move(input_files)),
      output_files_(std::move(output_files)) {
  MPI_Comm_size(communicator_, &world_size_);
  assert(reduced_num == output_files_.size());
  assert(reducer_num_ <= world_size_ - 1);
  for (size_t i = 0; i < reduced_num; ++i) {
    char buffer[L_tmpnam];
    files_to_reduce_.emplace_back(std::tmpnam(buffer));
  }
}

void MasterNode::Start() {
  SendInitialMaps();
  constexpr size_t max_buffer_size = 10000;
  std::vector<char> buffer(max_buffer_size);
  MPI_Status status;
  while (processed_reduces_ < reducer_num_) {
    MPI_Recv(buffer.data(), max_buffer_size, MPI_CHAR, MPI_ANY_SOURCE,
             MPI_ANY_TAG, communicator_, &status);
    ProcessMessage(buffer, &status);
  }
  TerminateWorkers();
}

void MasterNode::SendInitialMaps() {
  for (size_t i = 1; i < world_size_ && current_file_ < input_files_.size();
       ++i) {
    SendCurrentFileTo(i);
  }
}

void MasterNode::ProcessMessage(const std::vector<char> &buffer,
                                MPI_Status *status) {
  int source = status->MPI_SOURCE;
  if (!reduce_phase_) {
    SendCurrentFileTo(source);
    std::async(std::launch::async, &MasterNode::AddToReduceFiles, this,
               Common::SplitString(std::string(buffer.data())));
    ++processed_maps_;
    if (processed_maps_ == input_files_.size()) {
      StartReduce();
    }
  } else {
    ++processed_reduces_;
  }
}

void MasterNode::SendCurrentFileTo(int dest) {
  if (current_file_ < input_files_.size()) {
    const auto &str = input_files_[current_file_];
    auto task = "map " + str;
    MPI_Send(task.c_str(), task.size() + 1, MPI_CHAR, dest, 0, communicator_);
    ++current_file_;
  }
}

void MasterNode::AddToReduceFiles(const std::vector<std::string> &files) {
  assert(files.size() == reducer_num_);
  size_t i = 0;
  for (auto &guarded_file : files_to_reduce_) {
    std::ifstream temp_file_stream(files[i], std::ios_base::binary);
    std::lock_guard<std::mutex> lock(guarded_file.mutex);
    guarded_file.stream << temp_file_stream.rdbuf();
    guarded_file.stream.flush();
    temp_file_stream.close();
    fs::remove(files[i]);
    ++i;
  }
  ++real_processed_maps_;
  if (real_processed_maps_ == input_files_.size()) {
    can_start_.notify_one();
  }
}

void MasterNode::StartReduce() {
  std::unique_lock<std::mutex> lock(reduce_mutex_);
  can_start_.wait(lock, [this] {
    return real_processed_maps_.load() == input_files_.size();
  });
  reduce_phase_ = true;
  int i = 1;
  for (auto &guarded_file : files_to_reduce_) {
    const auto &task =
        "reduce " + guarded_file.filename + " " + output_files_[i - 1];
    guarded_file.stream.close();
    MPI_Send(task.c_str(), task.size() + 1, MPI_CHAR, i, 0, communicator_);
    ++i;
  }
}

void MasterNode::TerminateWorkers() {
  for (int i = 1; i < world_size_; ++i)
    MPI_Send("exit", 5, MPI_CHAR, i, 0, communicator_);
}

MasterNode::GuardedFile::GuardedFile(std::string name)
    : filename(std::move(name)),
      stream(filename, std::ios_base::app | std::ios_base::binary) {}
