//
// Created by sg on 13.09.2019.
//

#include "WorkerNode.hpp"
#include "BufferedChunkReader.hpp"
#include "BufferedLineReader.hpp"
#include "Common.hpp"
#include <cassert>
#include <filesystem>
#include <fstream>
#include <iterator>
#include <omp.h>
#include <sstream>

namespace fs = std::filesystem;

WorkerNode::WorkerNode(Mapper *mapper, Reducer *reducer, Reducer *combiner,
                       MPI_Comm communicator, size_t reduce_num)
    : mapper_(mapper), reducer_(reducer), combiner_(combiner),
      communicator_(communicator), reduce_num_(reduce_num),
      guards_(reduce_num_) {
  MPI_Comm_rank(communicator_, &world_rank);
  namespace ph = std::placeholders;
  mapper_->SetEmitCallback(std::bind(&WorkerNode::Emit, this, ph::_1, ph::_2));
}

void WorkerNode::Map(const std::string &input_file_) {
#pragma openmp parallel default(none) shared(input_file)
  {
    BufferedChunkReader reader(input_file_);
    while (!reader.Done()) {
      mapper_->Map(reader.ReadChunk());
    }
  }

  for (auto &ifs : map_output_)
    ifs.flush();
}

void WorkerNode::Emit(const std::string &key, const std::string &value) {
  static auto hasher = std::hash<std::string>{};
  auto hash = hasher(key) % reduce_num_;
  auto &ifs = map_output_[hash];
  std::lock_guard<std::mutex> lock(guards_[hash]);
  ifs << key << " " << value << "\n";
}

void WorkerNode::Reduce(const std::string &input_file_,
                        const std::string &output_file_,
                        Reducer *current_reducer) {
  ExternalSort(input_file_);
  BufferedLineReader reader(input_file_);
  std::ofstream ofs(output_file_);

  while (!reader.Done()) {
    auto line = reader.ReadLine();
    auto key = line.substr(0, line.find(' '));
    key.erase(std::remove_if(key.begin(), key.end(), isspace), key.end());
    if (key.empty())
      continue;
    current_reducer->SetEmitCallback([&key, &ofs](const std::string &value) {
      ofs << key << " " << value << "\n";
    });
    reader.GiveBack(line);
    ReduceInputIterator iterator(reader);
    current_reducer->Reduce(iterator);
  }
  ofs.flush();
  reader.Close();
  fs::remove(input_file_);
}

void WorkerNode::Start() {
  constexpr size_t max_buffer_size = 10000;
  std::vector<char> buffer(max_buffer_size);
  MPI_Status status;
  while (MPI_Recv(buffer.data(), max_buffer_size, MPI_CHAR, 0, 0, communicator_,
                  &status) == 0) {
    if (!ProcessMessage(buffer, &status))
      break;
  }
}

bool WorkerNode::ProcessMessage(const std::vector<char> &buffer,
                                MPI_Status *status) {
  auto str = std::string(buffer.data());
  auto input = Common::SplitString(str);
  if (str == "exit")
    return false;
  else if (input[0] == "map") {
    ProcessMap(input[1]);
  } else if (input[0] == "reduce") {
    Reduce(input[1], input[2], reducer_);
    MPI_Send("ok", 3, MPI_CHAR, 0, 0, communicator_);
  }
  return true;
}

void WorkerNode::ProcessMap(const std::string &input_file_) {
  assert(map_output_.empty());
  std::vector<std::string> output_files_;
  for (size_t i = 0; i < reduce_num_; ++i) {
    char buffer[L_tmpnam];
    output_files_.emplace_back(std::tmpnam(buffer));
    map_output_.emplace_back(output_files_.back());
  }
  Map(input_file_);
  map_output_.clear();
  if (combiner_) {
    for (size_t i = 0; i < reduce_num_; ++i) {
      char buffer[L_tmpnam];
      std::string reduce_file = std::tmpnam(buffer);
      Reduce(output_files_[i], reduce_file, combiner_);
      output_files_[i] = reduce_file;
    }
  }
  std::ostringstream stream;
  std::copy(output_files_.begin(), output_files_.end(),
            std::ostream_iterator<std::string>(stream, " "));
  auto message = stream.str();
  MPI_Send(message.c_str(), message.size() + 1, MPI_CHAR, 0, 0, communicator_);
}

void WorkerNode::ExternalSort(const std::string &input_file) {
  // TODO:: implement real external sort;
  std::vector<std::string> input;
  std::ifstream ifs(input_file);
  std::string str;

  while (!ifs.eof()) {
    std::getline(ifs, str, '\n');
    input.push_back(std::move(str));
  }
  ifs.close();

  std::sort(input.begin(), input.end());

  fs::resize_file(input_file, 0);
  std::ofstream ofs(input_file);
  std::copy(input.begin(), input.end(),
            std::ostream_iterator<std::string>(ofs, "\n"));
}
