//
// Created by sg on 13.09.2019.
//

#pragma once

#include "BufferedLineReader.hpp"
#include <fstream>
#include <mutex>

class ReduceInputIterator {
 public:
  explicit ReduceInputIterator(BufferedLineReader &reader_);

  bool Done();

  std::string Value();

  void Next();

  std::string Key();

  std::pair<std::string, std::string> KeyValue();

 private:
  bool UnsafeDone();

  std::string UnsafeValue();

  std::string UnsafeKey();

  void UnsafeNext();

 private:
  BufferedLineReader &reader_;
  std::string value_;
  std::string key_;
  bool done_{false};
  std::mutex mutex_;
};
