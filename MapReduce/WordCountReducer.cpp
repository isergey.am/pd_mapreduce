//
// Created by sg on 14.09.2019.
//

#include <cassert>
#include "WordCountReducer.hpp"

void WordCountReducer::Reduce(ReduceInputIterator &input) {
  unsigned long long value = 0;
#pragma omp parallel shared(input) default(none) reduction (+:value)
  {
    while (!input.Done()) {
      auto input_val = input.Value();
      if (input_val.empty())
        break;
      value += std::stoull(input_val);
      input.Next();
    }
  }
  // Emit sum for input->key()
  Emit(std::to_string(value));
}
