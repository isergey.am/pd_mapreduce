//
// Created by sg on 13.09.2019.
//

#include "ReduceInputIterator.hpp"
#include "Common.hpp"
#include <algorithm>

ReduceInputIterator::ReduceInputIterator(BufferedLineReader &reader)
    : reader_(reader) {}

bool ReduceInputIterator::UnsafeDone() {
  return (value_.empty() && reader_.Done()) || done_;
}

std::string ReduceInputIterator::UnsafeValue() {
  if (value_.empty())
    UnsafeNext();
  return std::move(value_);
}

void ReduceInputIterator::UnsafeNext() {
  if (reader_.Done())
    return;
  auto line = reader_.ReadLine();
  auto split_line = Common::SplitString(line);
  if (split_line.size()!=2) {
    UnsafeNext();
    return;
  }
  auto key = std::move(split_line[0]);
  value_ = std::move(split_line[1]);
  if (key_.empty())
    key_ = key;
  else if (key_!=key) {
    done_ = true;
    value_.clear();
    reader_.GiveBack(line);
  }
}

std::string ReduceInputIterator::UnsafeKey() {
  if (key_.empty())
    UnsafeNext();
  return key_;
}

bool ReduceInputIterator::Done() {
  std::unique_lock<std::mutex> lock(mutex_);
  return UnsafeDone();
}

std::string ReduceInputIterator::Value() {
  std::unique_lock<std::mutex> lock(mutex_);
  return UnsafeValue();
}

void ReduceInputIterator::Next() {
  std::unique_lock<std::mutex> lock(mutex_);
  if (!value_.empty())
    return;
  UnsafeNext();
}

std::string ReduceInputIterator::Key() {
  std::unique_lock<std::mutex> lock(mutex_);
  return UnsafeKey();
}

std::pair<std::string, std::string> ReduceInputIterator::KeyValue() {
  std::unique_lock<std::mutex> lock(mutex_);
  return {UnsafeKey(), UnsafeValue()};
}
