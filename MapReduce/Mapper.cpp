//
// Created by sg on 14.09.2019.
//

#include "Mapper.hpp"

void Mapper::Emit(const std::string &key, const std::string &value) {
  emit_callback(key, value);
}

void Mapper::SetEmitCallback(callback_t callback) {
  emit_callback = std::move(callback);
}
