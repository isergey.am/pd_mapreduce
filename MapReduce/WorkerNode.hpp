//
// Created by sg on 13.09.2019.
//

#pragma once

#include "Mapper.hpp"
#include "Reducer.hpp"
#include <mpi.h>
#include <mutex>

class WorkerNode {
public:
  WorkerNode(Mapper *mapper, Reducer *reducer, Reducer *combiner,
             MPI_Comm communicator, size_t reduce_num);

  void Start();

private:
  void Map(const std::string &input_file_);

  void Reduce(const std::string &input_file_, const std::string &output_file_,
              Reducer *current_reducer);

  void Emit(const std::string &key, const std::string &value);

  bool ProcessMessage(const std::vector<char> &buffer, MPI_Status *status);

  void ProcessMap(const std::string &input_file_);

  static void ExternalSort(const std::string &input_file);

private:
  Mapper *mapper_;
  Reducer *reducer_;
  Reducer *combiner_;
  MPI_Comm communicator_;
  size_t reduce_num_{0};
  int world_rank;
  std::vector<std::ofstream> map_output_;
  std::vector<std::mutex> guards_;
  std::vector<std::ofstream> reduce_output_;
};
