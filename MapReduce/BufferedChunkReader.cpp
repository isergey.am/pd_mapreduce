//
// Created by sg on 15.09.2019.
//

#include "BufferedChunkReader.hpp"

BufferedChunkReader::BufferedChunkReader(std::string file_name)
    : file_name_(std::move(file_name)), ifs_(file_name_) {
  buffer_.reserve(2*BufferSize);
}

const std::string &BufferedChunkReader::ReadChunk() {
  std::unique_lock<std::mutex> lock(mutex_);
  if (UnsafeDone())
    return buffer_;
  buffer_.resize(BufferSize);
  ifs_.read(buffer_.data(), BufferSize);

  if (ifs_.gcount()==0)
    return buffer_;

  size_t end = ifs_.gcount() - 1;
  buffer_.resize(ifs_.gcount());
  if (!isspace(buffer_[end]) && !ifs_.eof()) {
    std::string rest;
    ifs_ >> rest;
    buffer_ += rest;
  }
  return buffer_;
}

bool BufferedChunkReader::Done() {
  std::unique_lock<std::mutex> lock(mutex_);
  return UnsafeDone();
}

const size_t BufferedChunkReader::BufferSize = 10000;

bool BufferedChunkReader::UnsafeDone() {
  return ifs_.eof() || ifs_.fail();
}
