//
// Created by sg on 14.09.2019.
//

#include "WordCountMapper.hpp"

void WordCountMapper::Map(const std::string &text) {
  size_t n = text.size();
  for (int i = 0; i < n;) {
    // Skip past leading whitespace
    while ((i < n) && isspace(text[i]))
      i++;
    // Find word end
    int start = i;
    while ((i < n) && !isspace(text[i]))
      i++;
    if (start < i)
      Emit(text.substr(start, i - start), "1");
  }
}
