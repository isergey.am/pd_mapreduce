//
// Created by sg on 14.09.2019.
//

#pragma once

#include "Mapper.hpp"
#include "Reducer.hpp"
#include <memory>

class MapReduceSpecification {
public:
  MapReduceSpecification(Mapper::MapperType mapperType,
                         Reducer::ReducerType reducerType, size_t processNum,
                         size_t reducer_num);

  void AddInput(std::string input_file);

  void AddOutput(std::string output_file);

  void MapReduce();

  void SetMapperType(Mapper::MapperType mapperType);

  void SetReducerType(Reducer::ReducerType reducerType);

  void SetCombinerType(Reducer::ReducerType combinerType);

private:
  static void SplitFile(const std::string &file,
                        std::vector<std::string> &split_files);

  void RunWithMpi(const std::vector<std::string> &split_files);

private:
  std::vector<std::string> input_files_;
  std::vector<std::string> output_files_;
  Mapper::MapperType mapper_type_;
  Reducer::ReducerType reducer_type_;
  std::unique_ptr<Reducer::ReducerType> combiner_type_;
  static const size_t ChunkSize;
  size_t process_num_;
  size_t reducer_num_;
};
