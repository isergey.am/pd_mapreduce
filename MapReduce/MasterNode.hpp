//
// Created by sg on 12.09.2019.
//

#pragma once

#include <atomic>
#include <condition_variable>
#include <fstream>
#include <list>
#include <mpi.h>
#include <mutex>
#include <string>
#include <vector>

class MasterNode {
public:
  MasterNode(MPI_Comm communicator, size_t reduced_num,
             std::vector<std::string> input_files,
             std::vector<std::string> output_files);

  void Start();

  void ProcessMessage(const std::vector<char> &buffer, MPI_Status *status);

private:
  void SendInitialMaps();

  void SendCurrentFileTo(int dest);

  void AddToReduceFiles(const std::vector<std::string> &files);

  void StartReduce();

  struct GuardedFile {
    explicit GuardedFile(std::string name);

    std::string filename;
    std::ofstream stream;
    std::mutex mutex;
  };

private:
  MPI_Comm communicator_;
  size_t reducer_num_;
  std::vector<std::string> input_files_;
  std::vector<std::string> output_files_;
  int current_file_{0};
  int processed_reduces_{0};
  int processed_maps_{0};
  bool reduce_phase_{false};
  std::list<GuardedFile>
      files_to_reduce_; // use list for emplace_back without move
  int world_size_;
  std::condition_variable can_start_;
  std::mutex reduce_mutex_;
  std::atomic<int> real_processed_maps_{0};

  void TerminateWorkers();
};
