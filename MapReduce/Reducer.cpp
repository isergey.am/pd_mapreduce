//
// Created by sg on 13.09.2019.
//

#include "Reducer.hpp"

void Reducer::Emit(const std::string &value) { emit_callback(value); }

void Reducer::SetEmitCallback(Reducer::callback_t callback) {
  emit_callback = std::move(callback);
}
