//
// Created by sg on 15.09.2019.
//

#pragma once

#include <fstream>
#include <vector>
#include <mutex>

class BufferedChunkReader {
 public:
  explicit BufferedChunkReader(std::string file_name);

  const std::string &ReadChunk();

  bool Done();

 private:
  bool UnsafeDone();

 private:
  static const size_t BufferSize;
  std::string file_name_;
  std::ifstream ifs_;
  std::string buffer_;
  std::mutex mutex_;
};
