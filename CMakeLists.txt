cmake_minimum_required(VERSION 2.8)

set(CMAKE_CXX_COMPILER mpic++)
set(CMAKE_C_COMPILER mpicc)
set(CMAKE_CXX_STANDARD 17)

project(MapReduce)

find_package(MPI REQUIRED)

# set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${MapReduce_SOURCE_DIR}/bin)
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")

set(MAP_REDUCE_SOURCE MapReduce/MasterNode.hpp MapReduce/MasterNode.cpp MapReduce/WorkerNode.cpp MapReduce/WorkerNode.hpp MapReduce/Mapper.cpp MapReduce/Mapper.hpp MapReduce/Reducer.cpp MapReduce/Reducer.hpp MapReduce/ReduceInputIterator.cpp MapReduce/ReduceInputIterator.hpp MapReduce/BufferedLineReader.cpp MapReduce/BufferedLineReader.hpp MapReduce/Common.cpp MapReduce/Common.hpp MapReduce/WordCountMapper.cpp MapReduce/WordCountMapper.hpp MapReduce/WordCountReducer.cpp MapReduce/WordCountReducer.hpp MapReduce/BufferedChunkReader.cpp MapReduce/BufferedChunkReader.hpp MapReduce/MapReduceSpecification.hpp MapReduce/MapReduceSpecification.cpp)

add_library(MapReduceLib STATIC ${MAP_REDUCE_SOURCE})
target_include_directories(MapReduceLib PRIVATE ${MPI_CXX_INCLUDE_PATH})

add_executable(MapReduce MapReduce/map_reduce.cpp)
add_executable(WordCounter main.cpp MapReduce/MapReduceSpecification.cpp MapReduce/MapReduceSpecification.hpp)
add_executable(Test test/test.cpp test/test.hpp)
#target_compile_options(MapReduce PRIVATE ${MPI_CXX_COMPILE_FLAGS})

target_link_libraries(MapReduce MapReduceLib ${MPI_CXX_LIBRARIES} ${MPI_CXX_LINK_FLAGS} stdc++fs)
target_link_libraries(Test MapReduceLib gtest stdc++fs)
target_link_libraries(WordCounter stdc++fs)

if (UNIX)
    target_link_libraries(Test pthread)
endif ()
