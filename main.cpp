#include "MapReduce/MapReduceSpecification.hpp"

/*
 * Usage: ./WordCounter <process_num> <reducer_num>  output_files...,
 * input_files... Example: ./WordCounter 10 5 out1.txt out2.txt out3.txt
 * out4.txt out5.txt war-and-peace.txt
 */

int main(int argc, char **argv) {
  int process_num = strtol(argv[1], nullptr, 10);
  int reduce_num = strtol(argv[2], nullptr, 10);

  MapReduceSpecification spec(Mapper::MapperType::WordCount,
                              Reducer::ReducerType::WordCount, process_num,
                              reduce_num);

  for (int i = 3; i < reduce_num + 3; ++i)
    spec.AddOutput(argv[i]);

  for (int i = reduce_num + 3; i < argc; ++i)
    spec.AddInput(argv[i]);

  spec.SetCombinerType(Reducer::ReducerType::WordCount);

  spec.MapReduce();
}
